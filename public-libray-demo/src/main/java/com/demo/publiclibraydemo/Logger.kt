package com.demo.publiclibraydemo

import android.util.Log

class Logger {
    companion object {
        private const val TAG = "Logger"
        fun v(message: String) {
            Log.v(TAG, message)
        }
    }
}